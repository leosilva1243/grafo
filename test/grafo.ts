import { assert } from 'chai'

import { Grafo, EstrategiaGrafoNaoDirecionado, ArestaSemPeso } from '../grafo'

describe('Grafo', () => {
    function criarGrafo(): Grafo<ArestaSemPeso> {
        return new Grafo(new EstrategiaGrafoNaoDirecionado())
    }

    it('deveria conter o vertice após inserir', () => {
        const resultado = criarGrafo()
            .inserindoVertice('a')
            .contemVertice('a')
        assert.isTrue(resultado)
    })

    it('não deveria conter o vertice após remover', () => {
        const resultado = !criarGrafo()
            .inserindoVertice('a')
            .removendoVertice('a')
            .contemVertice('a')
        assert.isTrue(resultado)
    })

    it('deveria conter os vertices após inserir', () => {
        const grafo = criarGrafo()
            .inserindoVertice('a')
            .inserindoVertice('b')
        const vertices = grafo.vertices
        const resultado = vertices.indexOf('a') != -1
            && vertices.indexOf('b') != -1
        assert.isTrue(resultado)
    })

    it('não deveria conter os vertices após remover', () => {
        const grafo = criarGrafo()
            .inserindoVertice('a')
            .inserindoVertice('b')
            .removendoVertice('a')
            .removendoVertice('b')
        const resultado = grafo.vertices.size == 0
        assert.isTrue(resultado)
    })

    it('deveria conter a aresta após inserir', () => {
        const resultado = criarGrafo()
            .inserindoVertice('a')
            .inserindoVertice('b')
            .inserindoAresta({ origem: 'a', destino: 'b' })
            .contemAresta('a', 'b')
        assert.isTrue(resultado)
    })

    it('não deveria conter a aresta após remover', () => {
        const resultado = !criarGrafo()
            .inserindoVertice('a')
            .inserindoVertice('b')
            .inserindoAresta({ origem: 'a', destino: 'b' })
            .removendoAresta('a', 'b')
            .contemAresta('a', 'b')
        assert.isTrue(resultado)
    })

    it('deveria conter as arestas após inserir', () => {
        const grafo = criarGrafo()
            .inserindoVertice('a')
            .inserindoVertice('b')
            .inserindoVertice('c')
            .inserindoAresta({ origem: 'a', destino: 'b' })
            .inserindoAresta({ origem: 'a', destino: 'c' })
        const arestas = grafo.vizinhos('a')
        const resultado = arestas.indexOf('b') != -1
            && arestas.indexOf('c') != -1
        assert.isTrue(resultado)
    })

    it('não deveria conter as arestas após remover', () => {
        const grafo = criarGrafo()
            .inserindoVertice('a')
            .inserindoVertice('b')
            .inserindoVertice('c')
            .inserindoAresta({ origem: 'a', destino: 'b' })
            .inserindoAresta({ origem: 'a', destino: 'c' })
            .removendoAresta('a', 'b')
            .removendoAresta('a', 'c')
        const resultado = grafo.vizinhos('a').size == 0
        assert.isTrue(resultado)
    })
})