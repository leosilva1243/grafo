import { Grafo, ArestaComPeso } from './grafo'

export class BuscadorGrafo {
    private readonly estruturaBuscaFactory = new EstruturaBuscaFactory()
    buscar(grafo: Grafo<ArestaComPeso>, verticeInicial: string, verticeFinal: string | undefined, estrategia: EstrategiaBusca): Vertice[] {
        const estruturaBusca = this.estruturaBuscaFactory.criarEstrutura(estrategia)
        const vertices = grafo.vertices.toArray()
            .map(_vertice => {
                const vertice = new Vertice()
                vertice.vertice = _vertice
                vertice.verticePai = undefined
                vertice.custo = Infinity
                return vertice
            })
        let _verticeFinal: Vertice | undefined = undefined
        if (verticeFinal !== undefined) {
            _verticeFinal = this.vertice(verticeFinal, vertices)
        }
        let vertice: Vertice | undefined = this.vertice(verticeInicial, vertices)!
        vertice.custo = 0
        const visitado = [vertice]
        busca: while (vertice !== undefined) {
            const vizinhos = grafo.vizinhos(vertice.vertice).toArray()
                .map(vertice => this.vertice(vertice, vertices)!)
            for (const vizinho of vizinhos) {
                if (visitado.indexOf(vizinho) === -1) {
                    visitado.push(vizinho)
                    estruturaBusca.registrar(vizinho)
                    const custo = vertice.custo + grafo.peso(vertice.vertice, vizinho.vertice)
                    if (custo < vizinho.custo) {
                        vizinho.custo = custo
                        vizinho.verticePai = vertice.vertice
                    }
                    if (vizinho === _verticeFinal) break busca
                }
            }
            vertice = estruturaBusca.proximo()
        }
        return visitado
    }

    private vertice(vertice: string, vertices: Vertice[]): Vertice | undefined {
        return vertices.find(v => v.vertice === vertice)
    }
}

export class Vertice {
    vertice: string = ""
    verticePai: string | undefined = undefined
    custo: number
}

export enum EstrategiaBusca {
    breadthFirstSearch = 1, depthFirstSearch, dijkstra
}

class EstruturaBuscaFactory {
    criarEstrutura(estrategiaBusca: EstrategiaBusca): EstruturaBusca {
        switch (estrategiaBusca) {
            case EstrategiaBusca.breadthFirstSearch: return new EstruturaBuscaBFS()
            case EstrategiaBusca.depthFirstSearch: return new EstruturaBuscaDFS()
            case EstrategiaBusca.dijkstra: return new EstruturaBuscaDijkstra()
            default: throw 'Não implementado.'
        }
    }
}

interface EstruturaBusca {
    registrar(vertice: Vertice): void;
    proximo(): Vertice | undefined;
}

class EstruturaBuscaDFS implements EstruturaBusca {
    private vertices: Vertice[] = []

    registrar(vertice: Vertice) {
        this.vertices.push(vertice)
    }

    proximo(): Vertice | undefined {
        return this.vertices.pop()
    }
}

class EstruturaBuscaBFS implements EstruturaBusca {
    private vertices: Vertice[] = []

    registrar(vertice: Vertice) {
        this.vertices.push(vertice)
    }

    proximo(): Vertice | undefined {
        return this.vertices.shift()
    }
}

class EstruturaBuscaDijkstra implements EstruturaBusca {
    private vertices: Vertice[] = []

    registrar(vertice: Vertice) {
        this.vertices.push(vertice)
    }

    proximo(): Vertice | undefined {
        let menor: Vertice | undefined = undefined
        for (const vertice of this.vertices) {
            if (menor === undefined) {
                menor = vertice
                continue
            }
            if (vertice.custo < menor.custo) {
                menor = vertice
            }
        }
        if (menor !== undefined) {
            const indice = this.vertices.indexOf(menor)
            this.vertices.splice(indice, 1)
        }
        return menor
    }
}