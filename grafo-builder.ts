import { 
    Grafo, 
    Aresta, 
    ArestaComPeso, 
    ArestaSemPeso, 
    EstrategiaGrafo, 
    EstrategiaGrafoNaoDirecionado, 
    EstrategiaGrafoDirecionado 
} from './grafo'

class GrafoPrototype<TAresta extends Aresta> {
    orientado = false
    vertices: string[] = []
    arestas: TAresta[] = []
}

export class GrafoBuilder<TAresta extends Aresta> {
    private readonly _factory = new GrafoFactory()
    private readonly _parent?: GrafoBuilder<TAresta>

    constructor(parent?: GrafoBuilder<TAresta>) {
        this._parent = parent
    }

    protected modificar(grafo: GrafoPrototype<TAresta>) { }

    private modificar2(grafo: GrafoPrototype<TAresta>) {
        if (this._parent !== undefined) {
            this._parent.modificar2(grafo)
        }
        this.modificar(grafo)
    }

    build(): Grafo<TAresta> {
        var grafo = new GrafoPrototype<TAresta>()
        this.modificar2(grafo)
        return this._factory.criarGrafo(grafo)
    }

    adicionarVertice(vertice: string): GrafoBuilder<TAresta> {
        return new GrafoBuilderVertice(this, vertice)
    }

    adicionarAresta(aresta: TAresta): GrafoBuilder<TAresta> {
        return new GrafoBuilderAresta(this, aresta)
    }

    setOrientado(orientado: boolean): GrafoBuilder<TAresta> {
        return new GrafoBuilderOrientado(this, orientado)
    }
}

class GrafoBuilderAresta<TAresta extends Aresta> extends GrafoBuilder<TAresta> {
    private readonly _aresta: TAresta

    constructor(parent: GrafoBuilder<TAresta>, aresta: TAresta) {
        super(parent)
        this._aresta = aresta
    }
    
    protected modificar(grafo: GrafoPrototype<TAresta>) {
        grafo.arestas.push(this._aresta)
    }
}

class GrafoBuilderVertice<TAresta extends Aresta> extends GrafoBuilder<TAresta> {
    private readonly _vertice: string

    constructor(parent: GrafoBuilder<TAresta>, vertice: string) {
        super(parent)
        this._vertice = vertice
    }
    
    protected modificar(grafo: GrafoPrototype<TAresta>) {
        grafo.vertices.push(this._vertice)
    }
}

class GrafoBuilderOrientado<TAresta extends Aresta> extends GrafoBuilder<TAresta> {
    private readonly _orientado: boolean

    constructor(parent: GrafoBuilder<TAresta>, orientado: boolean) {
        super(parent)
        this._orientado = orientado
    }
    
    protected modificar(grafo: GrafoPrototype<TAresta>) {
        grafo.orientado = this._orientado
    }
}

class GrafoFactory {
    criarGrafo<TAresta extends Aresta>(prototype: GrafoPrototype<TAresta>): Grafo<TAresta> {
        let estrategia: EstrategiaGrafo
        if (prototype.orientado) estrategia = new EstrategiaGrafoDirecionado()
        else estrategia = new EstrategiaGrafoNaoDirecionado()
        const grafo = new Grafo<TAresta>(estrategia)
        for (const vertice of prototype.vertices) {
            grafo.inserirVertice(vertice)
        }
        for (const aresta of prototype.arestas) {
            grafo.inserirAresta(aresta)
        }
        return grafo
    }
}