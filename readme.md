# Trabalho 1.2 [Grafos]

## Sobre o trabalho

Desenvolvido para a matéria de Grafos com o professor Rodrigo Lyra. Os algorítmos presentes aqui foram criados para as seguintes tarefas:

* Permitir a criação de grafos **direcionados** e **não direcionados**
* Permitir a criação de arestas **sem peso** e **com peso**
* Permitir realizar a busca por **Breadth First Search**
* Permitir realizar a busca por **Depth First Search**
* Permitir realizar a busca por **Dijkstra**

## Criando um grafo

Para criar um grafo é necessário instanciar uma estratégia de grafo e passá-la para o construtor do grafo, além de especificar o tipo de aresta.

Exemplo:

``` typescript
const estrategia = new EstrategiaGrafoDirecionado()
const grafo = new Grafo<ArestaSemPeso>(estrategia)
```

As estratégias de grafo disponíveis são:

* `EstrategiaGrafoDirecionado`
* `EstrategiaGrafoNaoDirecionado`

Os tipos de aresta disponíveis são:

* `ArestaSemPeso`
* `ArestaComPeso`

## Realizando uma busca

Para realizar uma busca é necessário instanciar o `BuscadorGrafo` e chamar o método `buscar` passando os seguintes parâmetros: grafo, vertice inicial, vertice final (opcional), estratégia de busca.

Exemplo:

``` typescript
const buscador = new BuscadorGrafo()
buscador.buscar(grafo, 'a', 'z', EstrategiaBusca.breadthFirstSearch)
```

As estratégias de busca disponíveis são: 

* `EstrategiaBusca.breadthFirstSearch` 
* `EstrategiaBusca.depthFirstSearch` 
* `EstrategiaBusca.dijkstra`