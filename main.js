(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./grafo", "./buscador-grafo"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var grafo_1 = require("./grafo");
    var buscador_grafo_1 = require("./buscador-grafo");
    var buscadorGrafo = new buscador_grafo_1.BuscadorGrafo();
    var estrategiaAresta = new grafo_1.EstrategiaGrafoDirecionado();
    function criarGrafo(vertices, inicio) {
        var grafo = new grafo_1.Grafo(estrategiaAresta);
        vertices.forEach(function (vertice) {
            grafo.inserirVertice(vertice);
        });
        inserirArestas(inicio);
        function inserirArestas(origem) {
            var charCode = origem.charCodeAt(0);
            var destinos = [4, 3, 2, 1, -1, -2, -3, -4].map(function (c) { return String.fromCharCode(charCode + c); });
            for (var _i = 0, destinos_1 = destinos; _i < destinos_1.length; _i++) {
                var destino = destinos_1[_i];
                try {
                    grafo.inserirAresta({ origem: origem, destino: destino, peso: 1 });
                    if (!grafo.vizinhos(destino).contains(origem)) {
                        inserirArestas(destino);
                    }
                }
                catch (ex) { }
            }
        }
        return grafo;
    }
    var grafo = criarGrafo('abcdefghijklmnopqrstuvwxyz'.split(''), 'h');
    var estrategiasBusca = [
        buscador_grafo_1.EstrategiaBusca.breadthFirstSearch,
        buscador_grafo_1.EstrategiaBusca.depthFirstSearch,
        buscador_grafo_1.EstrategiaBusca.dijkstra
    ];
    for (var _i = 0, estrategiasBusca_1 = estrategiasBusca; _i < estrategiasBusca_1.length; _i++) {
        var estrategia = estrategiasBusca_1[_i];
        console.log();
        console.log(nomeEstrategiaBusca(estrategia) + ":");
        console.log();
        var resultado = buscadorGrafo.buscar(grafo, 'a', undefined, estrategia);
        console.log(serializarResultadoBusca(resultado));
        console.log();
    }
    function nomeEstrategiaBusca(estrategiaBusca) {
        switch (estrategiaBusca) {
            case buscador_grafo_1.EstrategiaBusca.breadthFirstSearch: return 'Breadth First Search';
            case buscador_grafo_1.EstrategiaBusca.depthFirstSearch: return 'Depth First Search';
            case buscador_grafo_1.EstrategiaBusca.dijkstra: return 'Dijkstra';
        }
    }
    function serializarResultadoBusca(resultado) {
        return resultado
            .map(function (vertice) { return "Vertice: " + vertice.vertice + "\tCusto: " + vertice.custo + "\tVerticePai: " + vertice.verticePai; })
            .join('\n');
    }
});
//# sourceMappingURL=main.js.map