import { List } from 'immutable'

export class Grafo<TAresta extends Aresta> {
    private tabela = List<List<number>>()
    private _vertices = List<string>()
    private _estrategia: EstrategiaGrafo

    constructor(estrategia: EstrategiaGrafo) {
        this._estrategia = estrategia
    }

    get vertices(): List<string> {
        return this._vertices
    }

    peso(origem: string, destino: string): number {
        const [i, j] = this.indicesVertices([origem, destino])
        return this._estrategia.peso(this.tabela, i, j)
    }

    vizinhos(label: string): List<string> {
        try {
            const [index] = this.indicesVertices([label])
            let retorno: string[] = []
            for (let j = 0; j < this.tabela.get(index).size; j++) {
                if (this._estrategia.peso(this.tabela, index, j) !== 0) {
                    retorno.push(this._vertices.get(j))
                }
            }
            return List(retorno)
        }
        catch (ex) {
            return List<string>()
        }
    }

    contemVertice(label: string): boolean {
        return this._vertices.indexOf(label) != -1
    }

    contemAresta(label0: string, label1: string): boolean {
        try {
            const [i, j] = this.indicesVertices([label0, label1])
            return this._estrategia.peso(this.tabela, i, j) !== 0
        }
        catch (ex) {
            return false
        }
    }

    inserindoVertice(label: string): Grafo<TAresta> {
        const copia = this.copia()
        copia.inserirVertice(label)
        return copia
    }

    removendoVertice(label: string): Grafo<TAresta> {
        const copia = this.copia()
        copia.removerVertice(label)
        return copia
    }

    inserindoAresta(aresta: TAresta): Grafo<TAresta> {
        const copia = this.copia()
        copia.inserirAresta(aresta)
        return copia
    }

    removendoAresta(label0: string, label1: string): Grafo<TAresta> {
        const copia = this.copia()
        copia.removerAresta(label0, label1)
        return copia
    }

    private static carregar<TAresta extends Aresta>(
        tabela: List<List<number>>,
        vertices: List<string>,
        estrategiaAresta: EstrategiaGrafo
    ): Grafo<TAresta> {
        const grafo = new Grafo(estrategiaAresta)
        grafo.tabela = tabela
        grafo._vertices = vertices
        return grafo
    }

    private copia(): Grafo<TAresta> {
        return Grafo.carregar(
            this.tabela,
            this._vertices,
            this._estrategia
        )
    }

    inserirVertice(label: string) {
        if (this.contemVertice(label)) {
            throw `Vertice '${label}' já presente no grafo`
        }

        this._vertices = this._vertices.push(label)
        for (let i = 0; i < this.tabela.size; i++) {
            this.tabela.update(i, linha => linha.push(0))
        }
        this.tabela = this.tabela.push(this._vertices.map(() => 0).toList())
    }

    removerVertice(label: string) {
        const [index] = this.indicesVertices([label])
        this._vertices = this._vertices.splice(index, 1).toList()
        for (let i = 0; i < this.tabela.size; i++) {
            this.tabela.update(i, linha => linha.splice(index, 1).toList())
        }
        this.tabela = this.tabela.splice(index, 1).toList()
    }

    inserirAresta(aresta: TAresta) {
        const [i, j] = this.indicesVertices([aresta.origem, aresta.destino])
        let peso = 1
        const arestaComPeso = aresta as ArestaComPeso
        if (arestaComPeso.peso !== undefined) {
            peso = arestaComPeso.peso
        }
        this.tabela = this._estrategia.atualizarPeso(this.tabela, i, j, peso)
    }

    removerAresta(label0: string, label1: string) {
        const [i, j] = this.indicesVertices([label0, label1])
        this.tabela = this._estrategia.atualizarPeso(this.tabela, i, j, 0)
    }

    indicesVertices(labels: string[]): number[] {
        const indexes: number[] = []
        for (const label of labels) {
            const index = this._vertices.indexOf(label)
            if (index == -1) throw `Vertice '${label}' não encontrado.`
            indexes.push(index)
        }
        return indexes
    }
}

export interface ArestaSemPeso {
    origem: string
    destino: string
}

export interface ArestaComPeso {
    origem: string
    destino: string
    peso: number
}

export type Aresta = ArestaSemPeso | ArestaComPeso

export interface EstrategiaGrafo {
    atualizarPeso(tabela: List<List<number>>, origem: number, destino: number, peso: number): List<List<number>>
    peso(tabela: List<List<number>>, origem: number, destino: number): number
}

export class EstrategiaGrafoDirecionado implements EstrategiaGrafo {
    atualizarPeso(tabela: List<List<number>>, origem: number, destino: number, peso: number): List<List<number>> {
        return tabela.update(origem, linha => linha.update(destino, elemento => peso))
    }

    peso(tabela: List<List<number>>, origem: number, destino: number): number {
        return tabela.get(origem).get(destino) || 0
    }
}

export class EstrategiaGrafoNaoDirecionado implements EstrategiaGrafo {
    atualizarPeso(tabela: List<List<number>>, origem: number, destino: number, peso: number): List<List<number>> {
        return tabela.update(origem, linha => linha.update(destino, elemento => peso))
            .update(destino, linha => linha.update(origem, elemento => peso))
    }

    peso(tabela: List<List<number>>, origem: number, destino: number): number {
        return tabela.get(origem).get(destino)
            && tabela.get(destino).get(origem) || 0
    }
}