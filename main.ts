import {
    Grafo,
    EstrategiaGrafoNaoDirecionado,
    EstrategiaGrafoDirecionado,
    ArestaSemPeso,
    ArestaComPeso
} from './grafo'
import { VerificadorPlanaridade } from './verificador-planaridade'
import { BuscadorGrafo, EstrategiaBusca, Vertice } from './buscador-grafo'

const buscadorGrafo = new BuscadorGrafo()
const estrategiaAresta = new EstrategiaGrafoDirecionado()
function criarGrafo(vertices: string[], inicio: string) {
    const grafo = new Grafo<ArestaComPeso>(estrategiaAresta)
    vertices.forEach(vertice => {
        grafo.inserirVertice(vertice)
    })
    inserirArestas(inicio)
    function inserirArestas(origem: string) {
        const charCode = origem.charCodeAt(0);
        const destinos = [4, 3, 2, 1, -1, -2, -3, -4].map(c => String.fromCharCode(charCode + c))
        for (const destino of destinos) {
            try {
                grafo.inserirAresta({ origem, destino, peso: 1 })
                if (!grafo.vizinhos(destino).contains(origem)) {
                    inserirArestas(destino)
                }
            }
            catch (ex) { }
        }
    }
    return grafo
}
const grafo = criarGrafo('abcdefghijklmnopqrstuvwxyz'.split(''), 'h')

const estrategiasBusca = [
    EstrategiaBusca.breadthFirstSearch,
    EstrategiaBusca.depthFirstSearch,
    EstrategiaBusca.dijkstra
]

for (const estrategia of estrategiasBusca) {
    console.log()
    console.log(`${nomeEstrategiaBusca(estrategia)}:`)
    console.log()
    const resultado = buscadorGrafo.buscar(grafo, 'a', undefined, estrategia)
    console.log(serializarResultadoBusca(resultado))
    console.log()
}

function nomeEstrategiaBusca(estrategiaBusca: EstrategiaBusca) {
    switch (estrategiaBusca) {
        case EstrategiaBusca.breadthFirstSearch: return 'Breadth First Search'
        case EstrategiaBusca.depthFirstSearch: return 'Depth First Search'
        case EstrategiaBusca.dijkstra: return 'Dijkstra'
    }
}

function serializarResultadoBusca(resultado: Vertice[]): string {
    return resultado
        .map(vertice => `Vertice: ${vertice.vertice}\tCusto: ${vertice.custo}\tVerticePai: ${vertice.verticePai}`)
        .join('\n')
}