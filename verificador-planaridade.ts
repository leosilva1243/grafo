import { List } from 'immutable'

import { Grafo } from './grafo' 

export class VerificadorPlanaridade {
    verificar(grafo: Grafo<any>): boolean {
        const v = this.quantidadeVertices(grafo)
        const a = this.quantidadeArestas(grafo)

        if (v <= 2) return true

        if (this.contemCiclosDeTamanho3(grafo)) {
            if (!(v >= 3 && a <= 3 * v - 6)) return false
        }
        else {
            if (!(v >= 3 && a <= 2 * v - 4)) return false
        }
        return true
    }

    private contemCiclosDeTamanho3(grafo: Grafo<any>): boolean {
        for (const vertice of grafo.vertices.toArray()) {
            const arestas = grafo.vizinhos(vertice)
            for (const vertice1 of arestas.toArray()) {
                const arestas1 = grafo.vizinhos(vertice1)
                for (const vertice2 of arestas1.toArray()) {
                    const arestas2 = grafo.vizinhos(vertice2)
                    if (arestas2.indexOf(vertice) != -1) return true
                }
            }
        }
        return false
    }

    private quantidadeVertices(grafo: Grafo<any>): number {
        return grafo.vertices.size
    }

    private quantidadeArestas(grafo: Grafo<any>): number {
        let arestas = grafo.vertices
            .map(v => grafo.vizinhos(v!).map(a => [v, a]))
            .reduce((arr, el) => arr!.concat(el!).toList(), List<[string, string]>())

        for (let i = 0; i < arestas.size; i++) {
            for (let j = i + 1; j < arestas.size; j++) {
                const [x, a] = arestas.get(i)
                const [b, y] = arestas.get(j)
                if (a == b && x == y) {
                    arestas = arestas.splice(j, 1).toList()
                }
            }
        }

        return arestas.size;
    }
}